package edu.ufl.facebook.routes

import edu.ufl.facebook.entity._
/**
 * Created by mebin on 11/20/15 for Facebook
 */
class ProfileRouter extends Router {
  def receive = runRoute(profileRoute)
  val profileRoute = {
    getJson {
      path("all"){
        complete(ProfileService.futureToJSONWait(ProfileRepository.findAll))
      }~
      path("allIds"){
        complete(ProfileService.futureToJSONWait(ProfileRepository.findAllIds))
      }~
      path(Segment) {
        id =>
          val f = ProfileRepository.findById(id)
          complete(ProfileService.futureToJSONWait(f))
      }
    }
  }
}

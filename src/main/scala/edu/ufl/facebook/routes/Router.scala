package edu.ufl.facebook.routes

import java.security.{PublicKey, MessageDigest}

import com.sun.deploy.net.DownloadEngine
import edu.ufl.facebook.Application
import edu.ufl.facebook.security.RSA
import org.apache.commons.codec.binary.Base64
import shapeless.HNil
import spray.http.{HttpRequest, HttpHeader, MediaTypes}
import spray.routing._
import scala.collection.mutable
import scala.collection.mutable._

/**
  * Created by mebin on 11/19/15 for Facebook
  */
trait Router extends HttpServiceActor {

  var clientKeys: Map[String, String] = HashMap[String, String]()

  def getJson(route: Route) = get {
    respondWithMediaType(MediaTypes.`application/json`) {
      //decrypt as well
      //encrypt this too or may be only one of them
      route
    }
  }

  def get(route: Route) = spray.routing.directives.MethodDirectives.get {

    //aaply encryption here
    route
  }

  def put(route: Route) = spray.routing.directives.MethodDirectives.put {

    //aaply encryption here
    route
  }

  def myRoute(route: Route): Route = headerValueByName("digitalSignature") { s =>
    ctx => {
      val body = ctx.request.message.entity.data.toByteArray
      val decDigitalSign = new String(RSA.decrypt(s, Application.loadedKeyPair.getPrivate))
      val bodyHashVal = getDigest(body)
      if (decDigitalSign.equals(bodyHashVal)) {
        println("Yes it matches")
      }
    }
      route
  }

  def signature(): Directive1[String] = {
    optionalHeaderValueByName("digitalSignature").flatMap {
      case Some(value) =>
        val data = new String(RSA.decrypt(value, Application.loadedKeyPair.getPrivate))
        provide(data)
      case None => provide("")
    }
  }

  def secure(route: Route) = {
    headerValueByName("digitalSignature") { digSign =>
      headerValueByName("userId") { userId =>
        var publicKey: String = null
        var data: String = null
        if (userId.equals("-1")) {
          data = RSA.decrypt(digSign, Application.loadedKeyPair.getPrivate)
        }
        else {
          publicKey = clientKeys(userId)
          data = ""
        }

        //val data = new String(RSA.decrypt(digSign, publicKey))

        entity(as[String]) { body =>
          if (getDigest(body.getBytes()).equals(data)) {
            route
          }
          else {
            ctx =>
              ctx.complete("Failed Authentication")
          }
        }
      }
    }
  }

  def data(): Directive1[Array[Byte]] = {
    extract(_.request.message.entity.data) flatMap {
      pa =>
        val dat = pa.toByteArray
        provide(dat)
    }
  }

  def post(route: Route) = spray.routing.directives.MethodDirectives.post {
    //secure {
    route
    //}
  }


  def doVerification(dSign: String, body: Array[Byte]): Directive1[Boolean] = {
    if (dSign.equals(getDigest(body))) {
      provide(true)
    }
    else {
      provide(false)
    }
  }

  def getDigest(body: Array[Byte]): String = {
    val md = MessageDigest.getInstance("SHA-256")
    md.update(body)
    val bodyHashed = md.digest()
    Base64.encodeBase64String(bodyHashed)
  }
}

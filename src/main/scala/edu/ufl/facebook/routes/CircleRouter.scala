package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity.{Circle, UserService}
import edu.ufl.facebook.security.RSA

/**
  * Created by Ashish on 12/14/2015.
  */
class CircleRouter extends Router {

  import spray.json._
  import DefaultJsonProtocol._

  def receive = runRoute(circleRoute)

  val circleRoute = {
    import spray.json._
    import DefaultJsonProtocol._
    getJson {
      path(Segment / "publicKey" / Segment) { (userId, circleId) =>
        val listOfCircles = Circle.idAndListOfCircles.get(userId)
        val circle = listOfCircles match {
          case Some(listOfCircle) => listOfCircle.filter((c: Circle) => c.id == circleId)
        }
        complete(circle(0).idAndEncKeyMap.get(userId) match {
          case Some(publicKeyAESKey) => publicKeyAESKey
          case None => s"Thenga for user ${userId}"
        })
      }
    } ~
      post {
        entity(as[String]) { encryptedAESKey =>
          path(Segment / "create") {
            userId =>
              val circleId = Utility.uuid
              Circle.addCircle(userId, Circle(circleId, userId, scala.collection.mutable.Map(userId -> encryptedAESKey)))
              complete(circleId)
          }
        }
      } ~
      getJson {
        path("ids") {
          headerValueByName("userId") { uId =>
            import scala.collection.mutable._
            val listOfIds: ListBuffer[String] = ListBuffer[String]()
            Circle.idAndListOfCircles(uId) foreach { circle => listOfIds += circle.id }
            println(s"The list of circle ids are ${listOfIds.toList.mkString}")
            complete(listOfIds.toList.toJson.toString())
          }
        }
      }~
    get{
      path("id"/Segment){id =>

        val listOfCircle = Circle.idAndListOfCircles.get(id) match {
          case Some(listBuffer) => listBuffer
        }
        var idAndEncMap = ""
        listOfCircle foreach (idAndEncMap += _.idAndEncKeyMap.mkString)
        complete(idAndEncMap.mkString)
      }
    }
  }
}

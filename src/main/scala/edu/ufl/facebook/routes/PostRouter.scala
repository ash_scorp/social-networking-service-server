package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity._


/**
  * Created by mebin on 11/20/15 for Facebook
  */
class PostRouter extends Router {

  def receive = runRoute(postRoute)

  val postRoute = {
    import edu.ufl.facebook.entity.PostJsonImplicits._
    import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
    getJson {
      path("all") {
        complete(PostService.futureToJSONWait(PostRepository.findAll))
      } ~
        path("allIds") {
          complete(PostService.futureToJSONWait(PostRepository.findAllIds))
        } ~
        path(Segment) {
          id =>
            val f = PostRepository.findById(id)
            complete(PostService.futureToJSONWait(f))
        }
    } ~
      post {
        path("save") {
          entity(as[Post]) { post =>
            val p = post.copy(id = Utility.uuid)
            val f = PostRepository.save(p)
            complete(PostService.futureToJSONWait(f))
          }
        }
      } ~
      post {
        path(Segment / "reply") { id => {
          entity(as[Post]) { post =>
            val result = PostService.addComments(id, post.id)
            if (result) {
              complete("Success")
            }
            else {
              complete("Faiure")
            }
          }
        }
        }
      } ~
      path(Segment / "Delete") { postId =>
        delete {
          complete(PostService.futureToJSONWait(PostRepository.delete(postId)))
        }
      }
  }
}

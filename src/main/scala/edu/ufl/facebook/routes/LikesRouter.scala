package edu.ufl.facebook.routes

import edu.ufl.facebook.Utility
import edu.ufl.facebook.entity.{Likes, LikesRepository, LikesService}
import edu.ufl.facebook.entity.LikesJsonImplicits._
import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller

/**
 * Created by mebin on 11/25/15 for Facebook
 */
class LikesRouter extends Router {
  def receive = runRoute(likesRoute)
  val likesRoute = {

    path(Segment ) { id =>
      getJson {
        val f = LikesRepository.findById(id)
        complete(LikesService.futureToJSONWait(f))
      } ~
        post {
          entity(as[Likes]) { like =>
            val lik = like.copy(id=Utility.uuid)
            val f = LikesRepository.save(lik)
            complete(LikesService.futureToJSONWait(f))
          }
        } ~
        delete {
            val f = LikesRepository.delete(id)
            complete(LikesService.futureToJSONWait(f))
        }
    }
  }
}

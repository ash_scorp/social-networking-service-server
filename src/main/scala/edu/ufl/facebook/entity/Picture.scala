package edu.ufl.facebook.entity

import spray.json.DefaultJsonProtocol

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by mebin on 11/20/15 for Facebook
 */
case class Picture(name: String, //The album ID
                   id: String, // The user provided caption given to this photo.
                   description: Option[String], // The description of the album
                   album: Option[String], //The album the photo is in
                   created_time: Option[String], //The time the photo was published
                   can_delete: Option[String], // A boolean indicating if the viewer can delete the photo
                   can_tag: Option[String], // A boolean indicating if the viewer can tag the photo
                   from: Option[String], //The profile (user or page) that uploaded this photo
                   link: Option[String], //A link to the photo on Facebook
                   location: Option[String], //Location associated with the photo, if any
                   //picture:Option[String], // A link to the 100px wide representing the photo

                   //Edges
                   likes: Option[List[String]], //People who like this
                   sharedPosts: Option[String], //People who share this
                   Comments: Option[String] //Comments made on it by self or other users
                    ) extends ID

object PictureJsonImplicits extends DefaultJsonProtocol {

  implicit val impPicture = jsonFormat13(Picture)
}

object PictureService extends Service[Picture] {

  def getPicture(id: String): Picture = {
    val u = PictureRepository.findById(id)
    Await.result(u, 2 milliseconds) match {
      case Some(pic: Picture) => pic
    }
  }

  def getPictureName(id: String): String = {
    getPicture(id).name
  }

  def getAlbumName(id: String): Option[String] = {
    getPicture(id).album
  }

  def getComments(id: String): Option[String] = {
    getPicture(id).Comments
  }

  def getLikes(id: String): Option[List[String]] = {
    getPicture(id).likes
  }

  def updateLike(picId: String, userId: String) = {
    val pic = PictureService.getPicture(picId)
    val n = pic.likes match {
      case Some(p) => userId :: p
    }
    val newPic = pic.copy(likes = Option(n))
    PictureRepository.update(newPic)
  }
}

object PictureRepository extends CRUDRepository[Picture]


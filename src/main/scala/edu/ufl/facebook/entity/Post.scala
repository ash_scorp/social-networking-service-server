package edu.ufl.facebook.entity

import akka.actor.Actor
import edu.ufl.facebook.entity.UserRepository._
import spray.json.DefaultJsonProtocol
import spray.routing.RequestContext
import scala.concurrent.duration._
import scala.collection.mutable
import scala.concurrent.{Await, Future}

/**
 * Created by mebin on 11/20/15 for Facebook
 */
case class Post(
                 id: String, // The id of the post
                 from: Profile, // Information about the profile that posted the message.
                 caption: Option[String], //The caption of the link in the post
                 created_time: Option[String], //The created time
                 description: Option[String], // A description of a link in the post.
                 link: Option[String], //The link attached to this post
                 message: Option[String], //the status message of this post
                 location: Option[String], // Any location attached to the post
                 shares: Option[String], // The shares count of this post.
                 updated_time: Option[String], // The time of the last change to this post, or the comments on it. For a post about a life event, this will be the date and time of the life event
                 replies: Option[List[String]] // The replies to the post or comment to it!!
               ) extends ID

object PostJsonImplicits extends DefaultJsonProtocol {
  import ProfileJsonImplicits._

  implicit val impPost = jsonFormat11(Post)
}

object PostService extends Service[Post] {

  def getPost(id:String): Post = {
    val f = PostRepository.findById(id)
    Await.result(f, 2 milliseconds)match {
      case Some(post:Post) =>  post
    }
  }

  def getCreator(id:String): Profile={
    getPost(id).from
  }

  def getMessage(id:String): Option[String] = {
    getPost(id).message
  }

  def updateMessage(id:String, newMessage:Option[String]): Boolean = {
    var uResult:Boolean = false
    val u = PostRepository.findById(id)
    var result = Await.result(u, 2 milliseconds)match {
      case Some(post:Post)=>
        val postCopy = post.copy(message =  newMessage)
        var f = PostRepository.update(postCopy)
        uResult = true
    }
    uResult
  }

  def addComments(id:String, commentId: String): Boolean = {
    var uResult:Boolean = false
    val fPost = PostRepository.findById(id)
    val res = Await.result(fPost, 2 milliseconds) match {
      case Some(p:Post) =>
        var j:List[String] = p.replies match {
          case Some(reply) =>  reply
        }
        j = commentId::j
        val pCopy = p.copy(replies = Option(j) )
        PostRepository.update(pCopy)
        uResult = true
    }
    uResult
  }

}

object PostRepository extends CRUDRepository[Post]

/*
class PostActor extends Actor {
  override def receive = {
    case ctx: RequestContext => ctx.complete("Welcome to Amber Gold!")
  }
}*/
package edu.ufl.facebook.entity

import spray.json.DefaultJsonProtocol
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by mebin on 11/20/15 for Facebook
 */
case class Album(name: String, //The album ID
                 id: String, // The id of the album
                 description: Option[String], // The description of the album
                 count: Int, //Number of photos in this album
                 place: Option[String], //The place associated with this album
                 event: Option[String], // The event associated with this album.
                 location: Option[String], // The textual location of the album.
                 userId: Int, //User or page id
                 privacy: Option[String], //The privacy of the album
                 created_time: Option[String], //The time the album was initially created.
                 updated_time: Option[String], //The last time the album was updated.

                 //Edges
                 likes: Option[Likes], // The number of likes in the album
                 comments: Option[List[String]], // Comments on the album
                 photos: Option[List[Picture]] // Photos contained in the album
                  ) extends ID

object AlbumJsonImplicits extends DefaultJsonProtocol {
  import LikesJsonImplicits._
  import PictureJsonImplicits._
  implicit val impAlbum = jsonFormat14(Album)
}

object AlbumService extends Service[Album]{

  def getAlbumDetails(id: String): Album = {
      var albumDetails:Album = null
      val u = AlbumRepository.findById(id)
      var result = Await.result(u, 2 milliseconds)match {
        case Some(album:Album) => albumDetails = album
      }
    albumDetails
  }

  def getPhots(id:String):Option[List[Picture]]={
      getAlbumDetails(id).photos
  }

  def getComments(id:String):Option[List[String]]={
    getAlbumDetails(id).comments
  }

  def getLikes(id:String):Option[Likes]={
    getAlbumDetails(id).likes
  }

  def updatePhotos(albumId:String, photo:Picture):Boolean ={
    val u = AlbumRepository.findById(albumId)
    Await.result(u, 2 seconds) match {
      case Some(alb:Album) => alb.photos match{
        case Some(p) =>
          val albumPics = photo::p
          val albumCopy = alb.copy(photos = Option(albumPics))
          AlbumRepository.update(albumCopy)
        case None =>
          val albumCopy = alb.copy(photos = Option(List(photo)))
          AlbumRepository.update(albumCopy)
          true
      }
      case None => false
    }
    false
  }
}

object AlbumRepository extends CRUDRepository[Album]

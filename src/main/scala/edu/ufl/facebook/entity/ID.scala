package edu.ufl.facebook.entity

/**
 * Created by mebin on 11/19/15 for Facebook
 */
/*
* Identifier trait, to be used for all objects as database identifier
 */
trait ID {
  def id: String
}

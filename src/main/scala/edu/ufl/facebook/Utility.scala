package edu.ufl.facebook
import scala.concurrent.duration._
import scala.concurrent.{Future, Await}

/**
 * Created by mebin on 11/29/15 for Facebook
 */
object Utility {
  def uuid = java.util.UUID.randomUUID.toString

  def waitForFuture(future:Future[Option[Any]]):Any = {
    Await.result(future ,2 milliseconds)
  }
}

Contributors: Ashish Nautiyal Mebin Jacob

This project was developed to implement RESTful service. The project is to develop a social networking platform like facebook, where people can come and share there posts, pages, create groups with other.

Key Features:
- Created RESTful web services using Spray-routing module.
- Implemented RSA-1024 and AES-128 along with IV for providing Authentication, Integrity and Confidentiality of user data.